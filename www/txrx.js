/**
 * TxRx Cordova plugin
 */

var exec = require('cordova/exec');

var txrx = {

    /**
     * scan()
     */
    scan: function () {
        exec(null, null, "TxrxPlugin", "startScan", []);
    },

    /**
     * connect()
     */
    connect: function (address) {
        exec(null, null, "TxrxPlugin", "connect", [address]);
    },

    /**
     * disconnect()
     */
    disconnect: function () {
        exec(null, null, "TxrxPlugin", "disconnect", []);
    },

    /**
     * readData()
     */
    readData: function (address) {
        exec(null, null, "TxrxPlugin", "readData", []);
    },

    /**
     * writeData()
     */
    writeData: function (data) {
        exec(null, null, "TxrxPlugin", "writeData", [data]);
    },

    /**
     * getTimeouts()
     */
    getTimeouts: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TxrxPlugin", "getTimeouts", []);
    },

    /**
     * setTimeouts()
     */
    setTimeouts: function (connectionTimeout, writeTimeout, firstReadTimeout, laterReadTimeout, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TxrxPlugin", "setTimeouts", [connectionTimeout, writeTimeout, firstReadTimeout, laterReadTimeout]);
    },

    /**
     * setDefaultTimeouts()
     */
    setDefaultTimeouts: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TxrxPlugin", "setDefaultTimeouts", []);
    },


    /**
     * Register callbacks
     */

    registerCallback: function(name, callback) {
        exec(callback, null, "TxrxPlugin", "registerCallback", [name]);
    }

};

module.exports = txrx;